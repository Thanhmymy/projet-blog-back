import { Router } from "express";
import passport from "passport";
import { Article } from "../entity/Article";
import { addPost, deletePost, findAll, findArticleByUser, findOnePost, updatePost } from "../repository/article-repository";


export const articleController = Router();


articleController.get('/all', async (req,resp)=>{
    let article = await findAll();
    resp.json(article)
})

articleController.post('/', passport.authenticate('jwt', {session:false}) , async (req,res)=>{
    let id = await addPost(req.body,req.user.id);
    res.status(201).json(req.body)
})

articleController.get('/one/:id', async (req,resp)=>{
    let article = await findOnePost(req.params.id);
    if(!article){
        resp.status(404).json({error : 'Pas trouvé'});
    return;
    }
    
    resp.json(article)
})

articleController.get('/user/:id', passport.authenticate('jwt',{session:false}), async (req,resp)=>{
    try {
        let article = await findArticleByUser(req.params.id);
        if(!article){
            resp.status(404).json({error : "pas trouvé"});
            return;
        }
        resp.json(article)
    } catch (error) {
        console.log(error);
    }
})

articleController.delete('/:id' ,passport.authenticate('jwt', {session:false}), async (req,resp)=>{
    try{
        let toDelete = await findOnePost(req.params.id);
        if(!toDelete){
            resp.status(404).end()
            return
        }
        if(req.user.id === toDelete.user_id){
            await deletePost(toDelete.id)
            resp.status(200).json(req.body)
        }else{
            resp.status(401).end()
        }
    }catch (e){
        console.log(e)
        resp.status(500).end()
    }
})

articleController.put('/update/:id', passport.authenticate('jwt',{session:false}), async (req,resp)=>{
    try{
        let toUpdate = await findOnePost(req.params.id);
        if(!toUpdate){
            resp.status(404).end()
            return
        }
        if(req.user.id === toUpdate.user_id){
            toUpdate.texte = req.body.texte
            await updatePost(toUpdate)
            resp.status(200).json(toUpdate)
        }else{
            resp.status(401).end()
        }
    }catch(e){
        console.log(e)
        resp.status(500).end()
    }
})

