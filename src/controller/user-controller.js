import { Router } from "express";
import { User } from "../entity/User";
import { findAllUser, findById, UserRepository } from "../repository/user-repository"
import bcrypt from 'bcrypt';
import { generateToken } from "../utils/token";
import passport from "passport";


export const userController = Router();

userController.post('/', async (req, res) => {
    try {
        const newUser = new User();
        Object.assign(newUser, req.body);

        const exists = await UserRepository.findByEmail(newUser.email);
        if (exists) {
            res.status(400).json({ error: 'Email already taken' });
            return;
        }

        newUser.role = 'user';

        newUser.password = await bcrypt.hash(newUser.password, 11);

        await UserRepository.add(newUser);
        res.status(201).json(newUser);

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});
userController.post('/login', async (req, res) => {
    try {

        const user = await UserRepository.findByEmail(req.body.email);
        if (user) {
            const samePassword = await bcrypt.compare(req.body.password, user.password);
            if (samePassword) {
                res.json({
                    user,
                    token: generateToken({
                        pseudo: user.pseudo,
                        email: user.email,
                        role: user.role,
                        id: user.id
                    })
                });
                return;
            }
        }
        res.status(401).json({ error: 'Wrong email and/or password' });
    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});
userController.get('/', passport.authenticate('jwt', { session: false }), (req, res) => {
    res.json({ message: "hello " + req.user.email });
});

userController.get('/all', async (req, resp) => {
    let user = await findAllUser();
    resp.json(user)
})
userController.get('/one/:id', async (req, resp) => {
    let user = await findById(req.params.id);
    if (!user) {
        resp.status(404).json({ error: 'pas trouvé' });
        return;
    }
    resp.json(user)
})

userController.get('/account', passport.authenticate('jwt', { session: false }), (req, res) => {

    res.json(req.user);
});
