export class Article{
    id;
    texte;
    user_id;

    /**
     * 
     * @param {String} texte 
     * @param {Number} user_id 
     * @param {Number} id 
     */
    constructor(texte,user_id, id=null){
        this.texte = texte;
        this.user_id = user_id
        this.id = id;
    }
}