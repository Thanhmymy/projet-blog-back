export class User{
    id;
    pseudo;
    email;
    password;
    role;

    /**
     * @param {string} pseudo
     * @param {string} email 
     * @param {string} password 
     * @param {string} role 
     * @param {number} id 
     */
    constructor(pseudo,email,password,role='user', id=null){
        this.pseudo = pseudo;
        this.email = email;
        this.password = password;
        this.role = role;
        this.id = id;
    }
}