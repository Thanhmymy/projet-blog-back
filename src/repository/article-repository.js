import { Article } from "../entity/Article";
import { connection } from "./connection";


export async function findAll(){
    const [rows]=await connection.query('SELECT * FROM article');
    const article = [];
    for(const row of rows){
        let instance = new Article(row.texte,row.user_id,row.id);
        article.push(instance);
    }
    return article;
}

export async function addPost(post,user_id){
    let [data]= await connection.query('INSERT INTO article (texte,user_id) VALUES (?,?)', [post.texte,user_id]);
    return post.id = data.insertId
}


export async function findOnePost(id){
    const [rows] = await connection.query('SELECT * FROM article WHERE id=?',[id]);
    if (rows.length === 1){
        return new Article(rows[0].texte,rows[0].user_id,rows[0].id);

    }
    return null;
}

export async function deletePost(id){
    const [rows] = await connection.query('DELETE FROM article WHERE id=?',[id])

}

export async function updatePost(parPost){
    const [rows] = await connection.query('UPDATE article SET texte=? WHERE id=?', [parPost.texte,parPost.id])
}

export async function findArticleByUser(id){
    const [rows] = await connection.query('SELECT * FROM article WHERE user_id=?',[id]);
    const article = [];
    for(const row of rows){
        let instance = new Article(row.texte,row.user_id,row.id);
        article.push(instance);
    }
    return article;
}