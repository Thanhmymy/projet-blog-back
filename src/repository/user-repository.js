import { User } from "../entity/User";
import { connection } from "./connection";


export class UserRepository {
    /**
     * 
     * @param {User} user
     */
    static async add(user) {
        const [rows] = await connection.query('INSERT INTO user (pseudo,email,password,role) VALUES (?,?,?,?)', [user.pseudo,user.email, user.password, user.role]);
        user.id= rows.insertId;
    }
    /**
     * 
     * @param {string} email 
     * @returns {Promise<User>} 
     */
    static async findByEmail(email) {
        const [rows] = await connection.query('SELECT * FROM user WHERE email=?', [email]);
        if(rows.length === 1) {
            return new User(rows[0].pseudo,rows[0].email, rows[0].password, rows[0].role, rows[0].id);
        }
        return null;

    }

}
export async function findAllUser(){
    const [rows]=await connection.query('SELECT * FROM user');
    const user = [];
    for(const row of rows){
        let instance = new User(row.pseudo,row.email,row.password,row.id);
        user.push(instance);
    }
    return user;
}

export async function findById(id){
    const [rows] = await connection.query ('SELECT * FROM user WHERE id=?',[id]);
    if (rows.length === 1){
        return new User(rows[0].pseudo,rows[0].email,rows[0].password,rows[0].role,rows[0].id);
    }
    return null;
}