import express from 'express';
import cors from 'cors';
import { userController } from './controller/user-controller';
import { articleController } from "./controller/article-controller"
import { configurePassport } from './utils/token';
import passport from 'passport';

configurePassport();

export const server = express();

server.use(passport.initialize());

server.use(express.json());
server.use(cors());
server.use(userController);
server.use(articleController)

server.use('/api/user', userController)
server.use('/api/article', articleController)

